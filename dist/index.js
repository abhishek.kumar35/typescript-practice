"use strict";
// Basic Types
let id = 5;
let company = "BITCS";
let isPublished = true;
let x = "hello";
x = false;
let age;
age = 30;
//array
let ids = [1, 2, 3, 4, 5];
let arr = [1, true, "hello"];
//Tuple
let person = [1, "Abhi", true];
//Tuple Array
let emploee;
emploee = [
    [1, "ABhi"],
    [2, "Marc"],
    [3, "Steven"],
];
//Union
let pid = 22;
pid = "22";
//Enum
var Direction1;
(function (Direction1) {
    Direction1[Direction1["Up"] = 0] = "Up";
    Direction1[Direction1["Down"] = 1] = "Down";
    Direction1[Direction1["Left"] = 2] = "Left";
    Direction1[Direction1["Right"] = 3] = "Right";
})(Direction1 || (Direction1 = {}));
var Direction2;
(function (Direction2) {
    Direction2["Up"] = "up";
    Direction2["Down"] = "down";
    Direction2["Left"] = "left";
    Direction2["Right"] = "right";
})(Direction2 || (Direction2 = {}));
// Objects
const user = {
    id: 1,
    name: "Jhon",
};
const user2 = {
    id: 1,
    name: "Marc",
};
// Type Assertion
let cid = '1';
let customerID = cid;
//or
let customerID2 = cid;
// Functions
function addNum(x, y) {
    return x + y;
}
// Void
function log(message) {
    console.log(message);
}
const user1 = {
    id: 1,
    name: 'Steven'
};
const add = (x, y) => x + y;
const sub = (x, y) => x - y;
// Classes
class Person {
    constructor(id, name) {
        this.id = id,
            this.name = name;
    }
    register() {
        return `${this.name} is now registered!`;
    }
}
const Marc = new Person(1, 'Marc Specter');
const Steven = new Person(1, 'Steven Grant');
// Inheritence or Subclasses
class Employee extends Person {
    constructor(id, name, position) {
        super(id, name);
        this.position = position;
    }
}
const emp = new Employee(3, 'tony', 'Developer');
// Generics
function getArrray(items) {
    return new Array().concat(items);
}
let numArray = getArrray([1, 2, 3, 4, 5]);
let strArray = getArrray(['one', 'two', 'three']);
