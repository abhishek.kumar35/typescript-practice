// Basic Types
let id: number = 5;
let company: string = "BITCS";
let isPublished: boolean = true;
let x: any = "hello";
x = false;

let age: number;
age = 30;

//array
let ids: number[] = [1, 2, 3, 4, 5];
let arr: any[] = [1, true, "hello"];

//Tuple

let person: [number, string, boolean] = [1, "Abhi", true];

//Tuple Array
let emploee: [number, string][];

emploee = [
  [1, "ABhi"],
  [2, "Marc"],
  [3, "Steven"],
];

//Union
let pid: string | number = 22;
pid = "22";

//Enum
enum Direction1 {
  Up, //0
  Down, //1
  Left, //2
  Right, //3
}

enum Direction2 {
  Up = "up",
  Down = "down",
  Left = "left",
  Right = "right",
}

// Objects
const user: {
  id: number;
  name: string;
} = {
  id: 1,
  name: "Jhon",
};

//OR

type User = {
  id: number;
  name: string;
};

const user2: User = {
  id: 1,
  name: "Marc",
};

// Type Assertion
let cid:any = '1'
let customerID = <number>cid
//or
let customerID2 = <number>cid


// Functions
function addNum(x:number,y:number):number {
    return x+y
}

// Void
function log(message:string|number):void{
    console.log(message);
}


//Interfaces

interface UserInterface{
    readonly id:number,
    name:string,
    age?:number,
}

const user1: UserInterface ={
    id:1,
    name:'Steven'
}

interface MathFUnc{
    (x:number, y:number):number
}

const add: MathFUnc = (x: number, y:number):number=>x+y
const sub: MathFUnc = (x: number, y:number):number=>x-y


// Iterfaces with class

interface PersonInterface{
    id:number,
    name:string,
    register():string
}

// Classes
class Person implements PersonInterface {
    id: number
    name:string

    constructor(id: number, name:string){
        this.id  = id,
        this.name = name
    }

    register(){
        return `${this.name} is now registered!`
    }
}

const Marc = new Person (1, 'Marc Specter')
const Steven = new Person (1, 'Steven Grant')


// Inheritence or Subclasses

class Employee extends Person {
    position:string

    constructor(id:number, name:string, position:string){
        super(id,name)
        this.position = position
    }
}

const emp = new Employee(3,'tony', 'Developer')


// Generics

function getArrray<T>(items:T[]):T[]{
    return new Array().concat(items)
}

let numArray = getArrray<number>([1,2,3,4,5])
let strArray = getArrray<string>(['one', 'two', 'three'])






